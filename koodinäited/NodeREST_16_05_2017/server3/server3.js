var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');

var hostname = 'localhost';
var port = 3000;

var app = express();
app.use(morgan('dev'));
app.use(bodyParser.json());

app.all('/continents', function(req, res, next){
    res.writeHead(200, {'Content-Type': 'text/plain'});
    next();
});

app.get('/continents', function(req, res, next){
    res.end('Will send all continents to you!');
});

app.post('/continents', function(req, res, next){
    res.end('Will add new continent with name'+ req.body.name +' and description ' + req.body.description+ '!');
});

app.delete('/continents/:continentId', function(req, res, next){
    res.end('Will delete continent with ID '+ req.params.continentId + '!');
});

app.use(express.static(__dirname + '/public'));

app.listen(port, hostname, function(){
    console.log(`Server is running at http://${hostname}:${port}/`)
});











