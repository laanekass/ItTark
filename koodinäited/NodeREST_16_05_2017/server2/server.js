var http = require('http');
var fs = require('fs');
var path = require('path');

var hostname = 'localhost';
var port = 3000;

var server = http.createServer(function(req, res){
    console.log('Request for url ' + req.url +' using method ' + req.method);
    
    var fileURL;
    if(req.url=='/'){
        fileURL = '/index.html';
    }else{
        fileURL = req.url;
    }
    
    var filePath = path.resolve('./public'+fileURL);
    var fileExtension = path.extname(filePath);
    if(req.method == 'GET') {
        if(fileExtension == '.html'){
            fs.exists(filePath, function(exists){
                if (!exists) {
                    res.writeHead(404, {'Content-Type': 'text/html'});
                    res.end('<html><body><h1>Error 404: ' + fileURL + ' not found!</h1></body></html>');
                    return;
                }
                res.writeHead(200, {'Content-Type': 'text/html'});
                fs.createReadStream(filePath).pipe(res);
            });
        } else {
            res.writeHead(404, {'Content-Type': 'text/html'});
            res.end('<html><body><h1>Error 404: ' + fileURL + ' not HTML file!</h1></body></html>');
            return;
        }
    }else{
        res.writeHead(404, {'Content-Type': 'text/html'});
            res.end('<html><body><h1>Error 404: ' + req.method + ' not supported!</h1></body></html>');
            return;
    }
});

server.listen(port, hostname, function(){
    console.log(`Server is running at http://${hostname}:${port}/`)
});