Select * 
from continent manner INNER JOIN country  riik
	ON manner.continent_id = riik.continent_id;
    
Select * 
from continent manner LEFT JOIN country  riik
	ON manner.continent_id = riik.continent_id;
    
Select * 
from continent manner RIGHT JOIN country  riik
	ON manner.continent_id = riik.continent_id;
    
Select * 
from continent manner LEFT JOIN country  riik
	ON manner.continent_id = riik.continent_id
UNION
Select * 
from continent manner RIGHT JOIN country  riik
	ON manner.continent_id = riik.continent_id;
    
Select riik.name as riigi_nimi
	, riik.population as elanike_arv
	, manner.name as mandri_nimi
from continent manner INNER JOIN country  riik
	ON manner.continent_id = riik.continent_id
where manner.name = 'Euroopa'
and riik.population >= 1500000;