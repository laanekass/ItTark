CREATE SCHEMA if not exists country_management3;

CREATE TABLE country_management3.continent (
continent_id INT(1) NOT NULL AUTO_INCREMENT, 
name varchar(20) NOT NULL , 
CONSTRAINT PK_continent PRIMARY KEY (continent_id)
);

CREATE TABLE country_management3.country (
country_id INT(3) NOT NULL AUTO_INCREMENT, 
name varchar(50) NOT NULL, 
population BIGINT(11),
continent_id INT(1),
created_time DATETIME DEFAULT CURRENT_TIMESTAMP,
climathe VARCHAR(20),
CONSTRAINT PK_country PRIMARY KEY (country_id),
CONSTRAINT UK_country_name UNIQUE KEY (name),
CONSTRAINT FK_country_continent_continent_id FOREIGN KEY (continent_id)
	REFERENCES country_management3.continent(continent_id)
);

INSERT INTO country_management3.continent (name) 
	VALUES ('Euroopa');
INSERT INTO country_management3.continent (name) 
	VALUES ('Lõuna-Ameerika');
    
INSERT INTO country_management3.country(name, population, continent_id)
	VALUES('Eesti', 1300000, 1);
    
INSERT INTO country_management3.country(name,
	population, continent_id, climathe)
	VALUES('Läti', 1900000, 1, 'parasvööde');
    
INSERT INTO country_management3.country(name,
	population, continent_id, climathe)
	VALUES('Brasiilia', 50000000, 
		(Select continent_id from continent 
		where name = 'Lõuna-Ameerika')
    , 'parasvööde');

Select * 
from country
Where population = (
	Select max(population) from country);
    
Select * 
from country
Where population in (
	Select min(population) from country
    union
    Select max(population) from country);
    
Select * 
from country
Where population > (
	Select avg(population) from country);
    
Select count(*) 
from country
Where population > (
	Select avg(population) from country);
    
Select lower(name)
from country;
    
Select name as riigi_nimi, LENGTH(name) as riigi_nime_pikkus
from country;
    