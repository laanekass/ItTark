var nimi = "Heleen",
    linn = "Tallinn",
    elanikkeLinnas = 400000,
    ALLA_26_KOEF = 2;
console.log(" Nimi on " + nimi + " ja elab linnas " + linn);
    nimi2 = "Mati";
    linn = "Tartu";
console.log(" Nimi on " + nimi2 + " ja elab linnas " + linn);

var linnad1 = ["Tallinn", "Tartu", "Pärnu"];
console.log("Linnad1="+linnad1);
console.log("Esimene = "+linnad1[0] + ", teine = " + linnad1[1] + ", kolmas = " + linnad1[2] + ", neljas = " + linnad1[3]);

var linnad2 = new Array("Tallinn", "Tartu", "Pärnu");
console.log("Linnad2="+linnad2);
console.log("Esimene = "+linnad2[0] + ", teine = " + linnad2[1] + ", kolmas = " + linnad2[2] + ", neljas = " + linnad2[3]);
linnad2[3] = "Viljandi";
console.log("Esimene = "+linnad2[0] + ", teine = " + linnad2[1] + ", kolmas = " + linnad2[2] + ", neljas = " + linnad2[3]);

var linnad3 = new Array();
console.log("linnad3 alguses  = " + linnad3);
linnad3[0] = "Tallilnn";
linnad3[1] = "Tartu";
linnad3[2] = "Pärnu";
console.log("Linnad3 pärast ="+linnad3);
console.log("Esimene = "+linnad3[0] + ", teine = " + linnad3[1] + ", kolmas = " + linnad3[2] + ", neljas = " + linnad3[3]);


function printNumber(givenNumber){
    console.log(givenNumber);
}

function sumNumbers(num1, num2){
    return num1 + num2;
}

function sayEvenOrOdd(givenNumber){
    if(givenNumber%2 == 0){                   
        document.getElementById("paarisPaaritu").innerHTML = givenNumber + "on paaris arv";
    }else{
        document.getElementById("paarisPaaritu").innerHTML = givenNumber + "on paaritu arv";
    }
}

function sayEvenOrOdd2(givenNumber){    
    document.getElementById("paarisPaaritu2").innerHTML = givenNumber%2 == 0 ? 
        givenNumber + "on paaris arv" : 
        givenNumber + "on paaritu arv";
}
