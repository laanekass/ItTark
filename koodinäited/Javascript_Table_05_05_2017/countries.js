var countries = {
    "countries" : [{
        "country" : {
            "id" : 0, 
            "name": "Estonia",
            "population" : 1300000,
            "continent" : {
                "id" : 0, 
                "name" : "Europe"
            }
        }
    },{
        "country" : {
            "id" : 1, 
            "name": "Latvia",
            "population" : 1980000,
            "continent" : {
                "id" : 0, 
                "name" : "Europe"
            }
        }
    },{
        "country" : {
            "id" : 2, 
            "name": "Canada",
            "population" : 35850000,
            "continent" : {
                "id" : 1, 
                "name" : "North-America"
            }
        }
    }
    ] 
};
console.log(countries);

console.log("JSONis on " + countries.countries.length + " rida");

function printCountries() {
    var tableContent = "";
    for(var index = 0; index < countries.countries.length; index++){
    
       tableContent = tableContent + "<tr><td>"+ countries.countries[index].country.name +"</td><td>" + countries.countries[index].country.population +"</td><td>"+ countries.countries[index].country.continent.name +"</td></tr>";
        
    }
    document.getElementById("countriesBody").innerHTML = tableContent;
}

function calculatePopulationSum(){
    var sum = 0;
    for(var index = 0; index < countries.countries.length; index++){
        sum += countries.countries[index].country.population;
        
        //järmine rida on samaväärne eelmisega
        //sum = sum +  countries.countries[index].country.population;
    }
    console.log(sum);
    return sum;
}

function printPopulationTotal() {
    document.getElementById("countriesFoot").innerHTML = "<tr><td><b>TOTAL</b></td><td>" + calculatePopulationSum() + "</td><td></td></tr>";
}
