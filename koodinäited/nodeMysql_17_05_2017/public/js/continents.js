var continents;
function fillAllContinentsTable() {
    console.log("funktsioon läks käima");
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:3000/continents",
        success: function(continentsResponse){
            continents = continentsResponse;
            var tableContent = "";
            console.log(continentsResponse);
            
            for(var i = 0; i < continentsResponse.length; i++) {
                tableContent = tableContent + '<tr><td>' + (i+1) + '</td><td>' 
                    + continentsResponse[i].name +  
                    '</td><td><input class="btn btn-default modify-continent" type="button" value="Muuda" data-toggle="modal" data-target="#modifyContinentModal"   onClick ="openModalWithData(' + continentsResponse[i].continent_id
                    + ')"><input class="btn btn-default modify-continent" type="button" value="Kustuta" data-toggle="modal" data-target="#deleteContinentModal"   onClick ="openDeleteContinentConfirmModal(' + continentsResponse[i].continent_id
                    + ')"></td></tr>';
            }
            document.getElementById("continentsTable").innerHTML = tableContent;
        }
    });
}

function addNewContinent() {
    var inputValue = document.getElementById("newContinentName").value;
    if(inputValue != "") {
        var newContinent = JSON.stringify({"name": inputValue});
        console.log(newContinent);
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: newContinent,
            url: "http://localhost:3000/continents",
            success: function(continentsResponse){
                console.log(continentsResponse);
                fillAllContinentsTable();
                showAlert("success-alert");
            }
        });
    }else{
        //showAlert("danger-alert");
        $("#danger-alert").alert();
        $("#danger-alert").fadeTo(2000, 500);
    }
}

function showAlert(alertId){
    $("#"+alertId).alert();
    $("#"+alertId).fadeTo(2000, 500).slideUp(500, function(){
        $("#"+alertId).slideUp(500);
    });
}

function saveContinentChanges() {
    var continentId = $(".modal-body #modifyContinentId").val();
    var continentName = $(".modal-body #modifyContinentName").val();
    if(continentName != ""){
        var modifiedContinent = JSON.stringify({"continent_id": continentId, "name": continentName});
        console.log(modifiedContinent);
        $.ajax({
            type: "PUT",
            dataType: "json",
            contentType: "application/json",
            data: modifiedContinent,
            url: "http://localhost:3000/continents",
            success: function(continentsResponse){
                console.log(continentsResponse);
                fillAllContinentsTable();                
                $('#modifyContinentModal').modal('hide');
                showAlert("modify-success-alert");
            }
        });
    }else{
        $("#modify-danger-alert").alert();
        $("#modify-danger-alert").fadeTo(2000, 500);
    }
}

function openModalWithData(selectedContinentId) {
    $("#modify-danger-alert").hide(); 
    for(var i= 0; i<continents.length; i++){
        if(continents[i].continent_id == selectedContinentId){
            $(".modal-body #modifyContinentId").val(selectedContinentId);
            $(".modal-body #modifyContinentName").val(continents[i].name);
        }
    }
};

var continentToDeleteId;
var continentToDeleteName;
function openDeleteContinentConfirmModal(selectedContinentId){
    for(var i= 0; i<continents.length; i++){
        if(continents[i].continent_id == selectedContinentId){
            continentToDeleteId = selectedContinentId;
            continentToDeleteName = continents[i].name;
            $(".modal-body #deleteMessage").text("Kas oled kindel, et soovid kustutada mandri " + continentToDeleteName + "?");
        }
    }
}

function deleteContinent() {
   if(continentToDeleteId != null){
        $.ajax({
            type: "DELETE",
            dataType: "json",
            contentType: "application/json",
            url: "http://localhost:3000/continents/" + continentToDeleteId,
            success: function(continentsResponse){
                fillAllContinentsTable();                
                $('#deleteContinentModal').modal('hide');
                showAlert("delete-success-alert");
            }
        });
    } 
}