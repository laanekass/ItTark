var currentUserName="";
function logInUser(userName, password) {
    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({"userName":userName, "password":password}),
        url: "http://localhost:3000/api/login",
        success: function(loginResponse){
            if(loginResponse.code == 200) {
                currentUserName = userName;
                if (typeof loginResponse.redirect == 'string') {
                    window.location = loginResponse.redirect;
                }
            }
        }
    });
}

function logout() {
    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify({"userName":currentUserName}),
        url: "http://localhost:3000/api/logout",
        success: function(loginResponse){
            if(loginResponse.code == 200) {
                if (typeof loginResponse.redirect == 'string') {
                    window.location = loginResponse.redirect;
                }
            }
        }
    });
}