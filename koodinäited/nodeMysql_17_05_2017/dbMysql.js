var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost', 
    user: 'root', 
    password: 'valiitdbAdmin_17!', 
    database: 'country_management3'
});

connection.connect();

exports.addNewContinent = function(continentName, callback){
    connection.query("INSERT INTO continent (name) VALUES ('" + continentName + "')", function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing query');
        }
    });
}

exports.getAllContinents = function(callback) {
    connection.query('SELECT * FROM continent', function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing query');
        }
    });
};

exports.deleteContinentById = function(continentId, callback) {
    connection.query('DELETE FROM CONTINENT WHERE continent_id = ' +continentId, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console('Error on performing delete query');
        }
    });
};

exports.updateContinent = function(continent, callback) {
    connection.query("UPDATE continent SET name = '" + continent.name + "' WHERE continent_id = " + continent.continent_id, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing update query');
        }
    });
};

exports.getUserByEmail = function(userEmail, callback) {
    connection.query('SELECT * FROM users WHERE email = ?',[userEmail], function (err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing query');
        }
    });
}