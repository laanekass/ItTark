var dbApis = require('./dbMysql');
var express = require('express');
var morgan = require('morgan');
var login = require('./routes/loginroutes');
var bodyParser = require('body-parser');
var session = require('express-session');
var router = express.Router();
var hostname = 'localhost';
var port = 3000;

var app = express();
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(session({
    secret: '2C44-4D44-WppQ38S',
    resave: true,
    saveUninitialized: true
}));

app.all('/continents', function(req, res, next){
    res.writeHead(200, {'Content-Type': 'application/json'});
    next();
});

app.get('/continents', function(req, res, next){
    dbApis.getAllContinents(function(continentsResponse){
        console.log(continentsResponse);
        res.end(JSON.stringify(continentsResponse));
    });    
});

app.post('/continents', function(req, res, next){ 
    console.log(req.body);
    dbApis.addNewContinent(req.body.name, function(continentsResponse){
        console.log(continentsResponse);
        res.end(JSON.stringify(req.body));
    });
});

app.put('/continents', function(req, res, next){    
    dbApis.updateContinent(req.body, function(continentsResponse){
        console.log(continentsResponse);
        res.end(JSON.stringify(req.body));
    });
});

app.delete('/continents/:continentId', function(req, res, next){
    dbApis.deleteContinentById(req.params.continentId, function(continentsResponse){
        console.log(continentsResponse);
        res.end('{"continent_id":'+ req.params.continentId + '}');
    });
});

// Authentication and Authorization Middleware
var auth = function(req, res, next) {
    var user="";
    dbApis.getUserByEmail(req.session.user, function(userResponse){
        if (userResponse.length == 1 && userResponse[0].email && req.session && req.session.admin) {
            return next();
        }else {
            return res.sendStatus(401);
        }
    });    
};

// Get content endpoint
app.get('/index.html', auth);

app.use(express.static(__dirname + '/public'));

router.post('/api/register',login.register);
router.post('/api/login',login.login)
router.post('/api/logout',login.logout)
app.use('/', router);



app.listen(port, hostname, function(){
    console.log(`Server is running at http://${hostname}:${port}/`)
});
