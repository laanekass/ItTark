var mysql      = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost', 
    user: 'root', 
    password: 'valiitdbAdmin_17!', 
    database: 'country_management3'
});
connection.connect(function(err){
if(!err) {
    console.log("Database is connected ... nn");
} else {
    console.log("Error connecting database ... nn");
}
});

exports.register = function(req,res){
  // console.log("req",req.body);
  var today = new Date();
  var users={
    "first_name":req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email,
    "password":req.body.password,
    "created":today,
    "modified":today
  }
  connection.query('INSERT INTO users SET ?',users, function (error, results, fields) {
  if (error) {
    console.log("error ocurred",error);
    res.send({
      "code":400,
      "failed":"error ocurred"
    })
  }else{
    console.log('The solution is: ', results);
    res.send({
      "code":200,
      "success":"user registered sucessfully"
        });
  }
  });
}

exports.login = function(req,res){
  var email= req.body.userName;
  var password = req.body.password;
  connection.query('SELECT * FROM users WHERE email = ?',[email], function (error, results, fields) {
  if (error) {
    res.send({
      "code":400,
      "failed":"error ocurred"
    })
  }else{
    if(results.length >0){
      if(results[0].password == password){
        req.session.user = email;
        req.session.admin = true;
          console.log("should redirect");
        res.send({
            "code": 200,
            "success": "login succeeded",
            "redirect": "http://localhost:3000/index.html"
        })
          //return res.redirect('/');
      }
      else{
        res.send({
          "code":204,
          "success":"Email and password does not match"
            });
      }
    }
    else{
      res.send({
        "code":204,
        "success":"Email does not exits"
          });
    }
  }
  });
}

exports.logout = function (req, res) {
  req.session.destroy();
  res.send({
            "code": 200,
            "success": "logout succeeded",
            "redirect": "http://localhost:3000/login.html"
        });
};