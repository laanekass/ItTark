CREATE TABLE IF NOT EXISTS citizen (
citizen_id INT(9) PRIMARY KEY AUTO_INCREMENT, 
first_name VARCHAR(25) NOT NULL, 
last_name VARCHAR(25) NOT NULL, 
country_id INT(3), 
CONSTRAINT FK_citizen_country_id 
	FOREIGN KEY(country_id) 
	REFERENCES country(country_id));

INSERT INTO citizen (first_name, last_name, country_id) 
	VALUES ('Mari', 'Maasikas', 1);
INSERT INTO citizen (first_name, last_name, country_id) 
	VALUES ('Mati', 'Kuusk', 2);
INSERT INTO citizen (first_name, last_name, country_id) 
	VALUES ('Jüri', 'Tamm', 1);
    
CREATE VIEW citizen_details AS
	SELECT concat(first_name, ' ', last_name) as name, 
		riik.name as country_name,
		manner.name as continent_name
	FROM citizen el INNER JOIN country riik ON
		(el.country_id = riik.country_id)
		INNER JOIN continent manner ON
		(riik.continent_id = manner.continent_id);
