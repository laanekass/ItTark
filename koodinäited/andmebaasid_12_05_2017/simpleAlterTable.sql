ALTER TABLE country ADD COLUMN average_rainfall INT(4);

ALTER TABLE country DROP COLUMN average_rainfall;

ALTER TABLE country ADD COLUMN average_rainfall INT(4)
	AFTER created_time;

