DELIMITER //
CREATE PROCEDURE migrate_db ()
BEGIN
	CALL remove_schema_and_tables();
    CALL create_schema_tables_migrate();
END//
Delimiter ;

DELIMITER //
CREATE PROCEDURE remove_schema_and_tables()
BEGIN
	IF EXISTS (select * from sys.schema_object_overview where db = 'country_management_migration') THEN
		ALTER TABLE country_management_migration.citizen 
			DROP FOREIGN KEY FK_citizen_country_id;
		ALTER TABLE country_management_migration.country 
			DROP FOREIGN KEY FK_country_continent_id;
		ALTER TABLE country_management_migration.country 
			DROP FOREIGN KEY FK_country_climathe_id;
	END IF;
	DROP TABLE IF EXISTS country_management_migration.citizen;
    DROP TABLE IF EXISTS country_management_migration.country;
    DROP TABLE IF EXISTS country_management_migration.climathe;
    DROP TABLE IF EXISTS country_management_migration.continent;
    
    -- tegelikult piisab ka ainult schema kustutamisest
    DROP SCHEMA IF EXISTS country_management_migration;
END//
Delimiter ;

Delimiter //
CREATE PROCEDURE create_schema_tables_migrate ()
BEGIN
	CREATE SCHEMA country_management_migration;
    
    CREATE TABLE country_management_migration.continent (
		continent_id INT(1) NOT NULL 
			AUTO_INCREMENT, 
		name varchar(20) NOT NULL , 
		CONSTRAINT PK_continent 
			PRIMARY KEY (continent_id)
	);
    
    CREATE TABLE country_management_migration.climathe (
		climathe_id INT(1) NOT NULL 
			AUTO_INCREMENT, 
		name varchar(20) NOT NULL , 
        avg_rainfall INT(4),
		CONSTRAINT PK_climathe 
			PRIMARY KEY (climathe_id)
	);
    
    CREATE TABLE country_management_migration.country (
		country_id INT(3) NOT NULL AUTO_INCREMENT, 
		name varchar(50) NOT NULL, 
		population BIGINT(11),
		continent_id INT(1),
		created_time DATETIME DEFAULT CURRENT_TIMESTAMP,
		climathe_id INT(1),
		CONSTRAINT PK_country PRIMARY KEY (country_id),
		CONSTRAINT UK_country_name UNIQUE KEY (name),
		CONSTRAINT FK_country_continent_id FOREIGN KEY (continent_id)
			REFERENCES country_management_migration.continent(continent_id),
		CONSTRAINT FK_country_climathe_id FOREIGN KEY (climathe_id)
			REFERENCES country_management_migration.climathe(climathe_id)
	);
    
    CREATE TABLE IF NOT EXISTS country_management_migration.citizen (
		citizen_id INT(9) PRIMARY KEY AUTO_INCREMENT, 
		first_name VARCHAR(25) NOT NULL, 
		last_name VARCHAR(25) NOT NULL, 
		country_id INT(3), 
		CONSTRAINT FK_citizen_country_id 
			FOREIGN KEY(country_id) 
			REFERENCES country_management_migration.country(country_id));
            
	INSERT INTO country_management_migration.continent (name)
		SELECT name FROM country_management3.continent;
        
	INSERT INTO country_management_migration.climathe (name)
		SELECT distinct(climathe) 
			FROM country_management3.country
			WHERE climathe is not null;
            
	INSERT INTO country_management_migration.country 
		(name, population, continent_id, created_time, climathe_id)
		SELECT vana_riik.name, 
			vana_riik.population,
			uus_manner.continent_id, 
			vana_riik.created_time,
			uus_kliima.climathe_id
		FROM country_management3.country vana_riik
			INNER JOIN country_management3.continent vana_manner
				on vana_riik.continent_id = vana_manner.continent_id
			INNER JOIN country_management_migration.continent uus_manner
				on vana_manner.name = uus_manner.name
			INNER JOIN country_management_migration.climathe uus_kliima
				on vana_riik.climathe = uus_kliima.name;
	
    INSERT INTO country_management_migration.citizen
		(citizen_id, first_name, last_name, country_id)
		SELECT citizen_id,
			first_name,
			last_name,
			uus_riik.country_id
		FROM country_management3.citizen vana_elanik
			INNER JOIN country_management3.country vana_riik
				ON vana_elanik.country_id = vana_riik.country_id
			INNER JOIN country_management_migration.country uus_riik
				on vana_riik.name = uus_riik.name;
END //
Delimiter ;