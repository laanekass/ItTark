var math = require('mathjs');
module.exports = function(radius, callback) {
    try {
        if(radius <=0){
            throw new Error("Radius has to be more than 0");
        }else if (radius > 50){
            throw new Error("Radius has to be less or equal than 50");
        }else {
            callback(null, {
                perimeter: function() {
                    return (2 * math.PI * radius);
                }, 
                area: function() {
                    return (math.PI*radius*radius);
                }
            });   
        }
    }catch(error) {
        callback(error, null);
    }
}