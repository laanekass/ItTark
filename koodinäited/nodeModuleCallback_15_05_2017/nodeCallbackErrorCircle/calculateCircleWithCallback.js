var argv = require('yargs')
                .usage('Usage: node $0 --radius=[num]')
                .demand(['radius'])
                .argv;

var rect = require('./circle');
function solveRect(radius){
    console.log("Circle radius is " + radius);
    
    rect(radius, function(err, circle) {
        if(err) {
                console.log(err);
        }else {
            console.log("circle area is " + circle.area());
            console.log("Circle perimeter is " + circle.perimeter());
        }
     });
};
solveRect(argv.radius);