var rect = require('./ristkylik')
function solveRect(sideA, sideB) {
    console.log("Teen tehted ristkülikuga, mille küljed on " + sideA + " ja " + sideB);
    if(sideA < 0 || sideB < 0){
        console.log("Ristküliku küljed peavad olema positiivsed arvud");
    }else{
        console.log(" Ristküliku pindala on " + rect.area(sideA, sideB));
        console.log(" Ristküliku ümbermõõt on " + rect.perimeter(sideA, sideB));
    }
}

solveRect(1, 3);
solveRect(8, 8);
solveRect(4, 3);
solveRect(-1, -3);