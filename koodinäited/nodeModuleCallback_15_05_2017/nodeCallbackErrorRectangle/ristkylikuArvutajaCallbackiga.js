var argv = require('yargs')
                .usage('Usage: node $0 --sideA=[num] --sideB=[num]')
                .demand(['sideA', 'sideB'])
                .argv;

var rect = require('./ristkylik2');
function solveRect(sideA, sideB){
    console.log("risküliku küljed on " + sideA + " ja " + sideB);
    
    rect(sideA, sideB, function(err, rectangle) {
         if(err) {
                console.log(err);
        }else {
            console.log("Ristküliku pindala on " + rectangle.area());
            console.log("Ristküliku ümbermõõt on " + rectangle.perimeter());
        }
     });
};
solveRect(argv.sideA, argv.sideB);