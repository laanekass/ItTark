module.exports = function(sideA, sideB, callback) {
    try {
        if(sideA <0 || sideB < 0){
            throw new Error("Risküliku küljed peavad olema positiivsed arvud");
        }else {
            callback(null, {
                perimeter: function() {
                    return (2*(sideA+sideB));
                }, 
                area: function() {
                    return (sideA*sideB);
                }
            });   
        }
    }catch(error) {
        callback(error, null);
    }
}