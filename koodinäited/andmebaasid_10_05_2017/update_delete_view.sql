UPDATE country_management3.continent
SET name = 'Euroopa2'
WHERE continent_id = 1;

Select * from continent;

UPDATE country
SET population = 3000000
WHERE name = 'Eesti';

Select * from country;

UPDATE  country
SET climathe = 'parasvööde'
WHERE continent_id = (
	SELECT continent_id
    FROM continent
    WHERE name = 'Euroopa2');
    
Select name, 
	population, 
	CASE
		WHEN population < 2000000 THEN 'väike'
		WHEN population > 10000000 THEN 'suur'
		ELSE 
			'keskmine'
	END as riigi_tyyp
from country;

Select name, 
	population, 
	CASE
		WHEN population < 2000000 THEN 'väike'
		WHEN population > 10000000 THEN 'suur'
		WHEN population 
			BETWEEN 2000000 AND 10000000 THEN 'keskmine'
	END as riigi_tyyp
from country;

Select name, 
	population, 
	CASE
		WHEN population < 2000000 THEN 'väike'
		WHEN population > 10000000 THEN 'suur'
        WHEN population >= 2000000 
			AND population <= 10000000 THEN 'keskmine'
	END as riigi_tyyp
from country;


Select CONCAT(country_id,  '. ', name) as riik, 
	population as elanike_arv, 
	CASE
		WHEN population < 2000000 THEN CONCAT(name, ' on väike')
		WHEN population > 10000000 THEN CONCAT(name, ' on suur')
        WHEN population >= 2000000 
			AND population <= 10000000 THEN CONCAT(name, ' on keskmine')
	END as riigi_tyyp
from country;


SET @row_number = 0;
Select CONCAT(@row_number := @row_number + 1
				,  '. ', name) as riik, 
	population as elanike_arv, 
	CASE
		WHEN population < 2000000 THEN CONCAT(name, ' on väike')
		WHEN population > 10000000 THEN CONCAT(name, ' on suur')
        WHEN population >= 2000000 
			AND population <= 10000000 THEN CONCAT(name, ' on keskmine')
	END as riigi_tyyp
from country;

DELETE FROM continent 
WHERE name = 'kustutan_ara 1';

DELETE FROM continent 
WHERE continent_id = 4;

TRUNCATE continent;

DROP TABLE continent;


CREATE OR REPLACE VIEW country_infrmation AS
	SELECT  riik.name as riigi_nimi,
			riik.population as elanike_arv,
			manner.name as manner
	FROM  country riik INNER JOIN continent manner
	  ON  riik.continent_id = manner.continent_id;

CREATE OR REPLACE VIEW country_infrmation 
	(riigi_nimi, elanike_arv2, manner, pealinn) AS
	SELECT  riik.name, 
			riik.population,
			manner.name,
            null
	FROM  country riik INNER JOIN continent manner
	  ON  riik.continent_id = manner.continent_id;

